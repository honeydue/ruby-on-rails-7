class HomeController < ApplicationController

  def index
    # Display welcome page
    render 'home/index'
  end

end